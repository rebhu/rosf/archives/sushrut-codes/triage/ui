import { RouteConfig } from 'vue-router'

const routes: RouteConfig[] = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Index.vue') },
      { path: 'privacy', component: () => import('pages/Privacy.vue') },
      { path: 'ToS', component: () => import('pages/ToS.vue') },
      { path: 'new/', component: () => import('pages/NewCylinder.vue') },
      { path: 'search/', component: () => import('pages/SearchCylinder.vue') },
      { path: ':id/request', component: () => import('pages/CreateCylinderRequest.vue') },
      { path: ':id', component: () => import('pages/ShowCylinder.vue') }
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '*',
    component: () => import('pages/Error404.vue')
  }
]

export default routes
